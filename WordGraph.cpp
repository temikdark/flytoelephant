//
// Created by darkness on 26.12.15.
//

#include <vector>
#include <iostream>
#include "WordGraph.h"

int isSimilar(std::string first, std::string second){
        if (first.length() != second.length()) {
                return -1;
        }
        int result = 0;
        for (int i = 0; i < first.length(); ++i) {
                if (first[i] != second[i]) {
                        ++result;
                }
        }
        return result;
}

Node::Node(const std::string &_name) : name(_name) {}

bool Node::addNext(const Node &new_node) {
        if (isSimilar(new_node.name, name) == 1) {
                next.insert(new_node.name);
                return true;
        }
        return false;
}

std::set<std::string> Node::getNext() const {
        return next;
}

bool Node::operator<(const Node &b) const {
        return name < b.name;
}

void WordGraph::addNode(const std::string &new_node_name) {
        Node new_node(new_node_name);
        std::vector<Node> tmp;
        for (auto node : nodes) {
                node.addNext(new_node);
                new_node.addNext(node);
                tmp.push_back(node);
        }
        nodes = std::set<Node>(tmp.begin(),tmp.end());
        nodes.insert(new_node);
}

Node WordGraph::findNode(const std::string& name) const {
        auto tmp = nodes.find(Node(name));
        if (tmp == nodes.end()) {
                return Node("");
        }
        return *tmp;
}

std::list<std::string> WordGraph::checkPath(const std::string &first, const std::string &second) const {
        std::set<std::string> watched;
        std::list<std::string> to_watch;
        std::list<std::string> path;
        to_watch.push_back(first);
        while (true) {
                if (to_watch.empty()) {
                        return {};
                }
                std::string tmp(to_watch.back());
                path.push_back(tmp);
                watched.insert(tmp);
                switch (isSimilar(tmp, second)) {
                        case -1: {
                                return {};
                        }
                        case 1: {
                                path.push_back(second);
                        }
                        case 0: {
                                return path;
                        }
                        default: {
                                for (const std::string &name : findNode(tmp).getNext()) {
                                        if (watched.find(name) == watched.end()) {
                                                to_watch.push_back(name);
                                        }
                                }
                                if (isSimilar(to_watch.back(), tmp) == 0) {
                                        to_watch.pop_back();
                                        path.pop_back();
                                }
                        }
                }
        }
}