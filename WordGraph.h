//
// Created by darkness on 26.12.15.
//

#ifndef FLYTOELEFANT_WORDGRAPH_H
#define FLYTOELEFANT_WORDGRAPH_H

#include <string>
#include <set>
#include <list>

int isSimilar(std::string, std::string);

class Node {
 private:
        std::string name;
        std::set<std::string> next;
 public:
        Node(const std::string &);
        bool addNext(const Node &);
        std::set<std::string> getNext() const;
        bool operator<(const Node&) const;
};

class WordGraph {
private:
        std::set<Node> nodes;
public:
        void addNode(const std::string &);
        Node findNode(const std::string&) const;
        std::list<std::string> checkPath(const std::string &, const std::string &) const;
};

#endif //FLYTOELEFANT_WORDGRAPH_H
