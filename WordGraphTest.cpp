//
// Created by darkness on 26.12.15.
//
#define BOOST_TEST_MODULE testwg

#include <boost/test/unit_test.hpp>
#include "WordGraph.h"


BOOST_AUTO_TEST_CASE(ZeroWordGraph) {
        WordGraph a;
        BOOST_CHECK(!a.checkPath("", "").empty());
}

BOOST_AUTO_TEST_CASE(OneStepWordGraph) {
        WordGraph a;
        BOOST_CHECK(!a.checkPath("a", "b").empty());
}

BOOST_AUTO_TEST_CASE(ExistPathWordGraph) {
        WordGraph a;
        a.addNode("aaa");
        a.addNode("aba");
        a.addNode("dba");
        BOOST_CHECK(!a.checkPath("aaa", "dbc").empty());
}

BOOST_AUTO_TEST_CASE(NoWordGraph) {
        WordGraph a;
        a.addNode("aaaa");
        a.addNode("abaa");
        a.addNode("dbaa");
        a.addNode("abca");
        BOOST_CHECK(a.checkPath("aaaa", "dddd").empty());
}
