#include <iostream>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include "WordGraph.h"


int main(int argc, char* argv[]) {
        if (argc != 3) {
                std::cerr << "Needed 2 arguments" << std::endl;
                return -1;
        }
        WordGraph dict;
        std::ifstream dict_file(argv[1], std::fstream::in);
        while (dict_file.good()) {
                std::string word;
                dict_file >> word;
                dict.addNode(boost::to_upper_copy<std::string>(word));
        }
        dict_file.close();
        std::ifstream check_file(argv[2], std::fstream::in);
        if (!check_file.good()) {
                std::cerr << "Issues while opening check file" << std::endl;
                return -1;
        }
        std::string first, second;
        check_file >> first >> second;
        for (auto word : dict.checkPath(boost::to_upper_copy<std::string>(first),
                                        boost::to_upper_copy<std::string>(second))) {
                std::cout << word << std::endl;
        }
        return 0;
}